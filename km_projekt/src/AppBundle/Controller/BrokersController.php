<?php

	namespace AppBundle\Controller;
	
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Component\HttpFoundation\Response;
	use BrokersDane;
		
	class BrokersController
	{
		
		/**
		 * @Route("/brokers")
		 */
		 
		public function pokazBrokerow()
		{
			$DaneBrok = new BrokersDane();
			return new Response(
				$DaneBrok->wyswietlDane()
			);
		}
	}
?>