<?php
	use GuzzleHttp\Client;
		
	class BrokersDane {

		public $brokres;
		public $baseURI = 'https://eie.jippioo.no/api/';
		
		public function __construct(){
			    $client = new Client(['base_uri' => $this->baseURI]);
				$response = $client->request('GET', 'brokers');
				$body = $response->getBody();
				$vdane = json_decode($body);
				$this->brokers = $vdane->{'data'}->{'brokers'};							
		}
		
		
				
		public function wyswietlDane(){
				
				$tableHTML = '<table>';
					for($i = 0; $i < count($this->brokers); $i++){					
						$tableHTML .= '<tr>'
						.'<td>'.$this->brokers[$i]->{'id'}.'</td>'
						.'<td>'.$this->brokers[$i]->{'name'}.'</td>'
						.'<td>'.$this->brokers[$i]->{'office'}.'</td>'
						.'<td>'.$this->brokers[$i]->{'phone'}.'</td>'
						.'<td>'.$this->brokers[$i]->{'email'}.'</td>'
						.'<td>'.$this->brokers[$i]->{'description'}.'</td>'
						.'<td>'.$this->brokers[$i]->{'imageUrl'}.'</td>'
						.'<tr>';
					}
				$tableHTML .= '</table>';

			return $tableHTML;
		}
	}
?>